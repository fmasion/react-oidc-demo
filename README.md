# POC React oidc-client.js 

## Setup

### dans un 1er terminal : 

```
./app_up.sh
```

### dans un 2nd terminal :

```
nvm use v8.15.0
cd src
yarn install
yarn start
```

## Utilisation :

La connection à l'admin de keycloak sur `http://localhost:9080/auth/admin`

    user : admin
    pass : admin

 l'app réact sur `http://localhost:3000` se loggue avec les identifiants :   

    user : user
    pass : user 


## Notes pour la customisation : 

Le fichier `src/helpers/Constants.ts` contient toutes les configurations de l'IdP
Par défaut keycloak est utilisé.
Pour changer d'IdP il suffit de paramétrer un IdP dans ce fichier.

`https://demo.identityserver.io/` fonctionne très bien en standalone (sur la branche master)

Voili voilou


