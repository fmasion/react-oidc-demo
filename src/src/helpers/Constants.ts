
// Keycloak docker
export class Constants {
  public static stsAuthority = 'http://localhost:9080/auth/realms/jhipster/';
  public static clientId = 'web_app';
  public static clientRoot = 'http://localhost:3000/';
  public static clientScope = 'openid profile email api';

  public static apiRoot = 'http://localhost:8080/s1/s2/s3?a=1&a=2&b=toto&next_uri=http://api:8080';
}

/*
// google
export class Constants {
  public static stsAuthority = 'https://accounts.google.com/';
  public static clientId = 'homeserve'; //???
  public static clientRoot = 'http://localhost:3000/';
  public static clientScope = 'openid profile email api';

  public static apiRoot = 'http://localhost:8080/s1/s2/s3?a=1&a=2&b=toto&next_uri=http://api:8080';
}
*/
/*
// Identityserver.io
export class Constants {
  public static stsAuthority = 'https://demo.identityserver.io/';
  public static clientId = 'implicit';
  public static clientRoot = 'http://localhost:3000/';
  public static clientScope = 'openid profile email api';

  public static apiRoot = 'https://jsonplaceholder.typicode.com/todos/1';
}
*/

// https://accounts.google.com