import * as React from 'react';
import ReactJson from 'react-json-view';


export interface IAuthContentProps {
  api: any;
  user: any;
}

export default class AuthContent extends React.Component<IAuthContentProps, any> {
  public shouldExpandNode = (keyPath: Array<string | number>, data: [any] | {}, level: number) => {
    return true;
  };

  public render() {
    return (
      <div className="row">
        <div className="col-md-6">
          <ReactJson src={this.props.user || {}} name="User Profile" theme="monokai" displayDataTypes={false} collapseStringsAfterLength={40} />
        </div>
        <div className="col-md-6">
          <ReactJson src={this.props.api} name="Api Response" theme="monokai" displayDataTypes={false} collapseStringsAfterLength={40}/>
        </div>
      </div>
    );
  }
}
